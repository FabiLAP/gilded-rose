package gildedrose;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

@RunWith(Parameterized.class)
public class GildedRoseTest {

	@Parameters
	public static Collection<	Object[]> data() {
		Collection<Object[]> data = new ArrayList<Object[]>();
		data.addAll(Arrays
				.asList(new Object[][] { 
						//test case default
						{"TEST-CASE","Item1", 5, 49, 4, 48 },
						//message
						//item Name
						
						//sell in value
						//quality
						//expected sell in
						//expected quality
						
						//START//
						{"At the end of each day our system lowers both quality and sell-in for every item",
						 "Item with arbitrary name", 5, 49, 4, 48 },
						
						{"sell in expired","Item with arbitrary name", 0, 25, -1, 23 },
						
						{"sell in expired","Item with arbitrary name", -2, 25, -3, 23 },
						
						{"quality is already 0","Item with arbitrary name", 1, 0, 0, 0 },
						
						//##################changed characterization test on the basis of the requirements
						// characterization test was (1,-1,0,-1)
						{"quality is negative","Item with arbitrary name", 1, -1, 0, 0 },
						
						//##################changed characterization test on the basis of the requirements
						// characterization test was (1, 55, 0, 54 )
						{"quality above 50","Item with arbitrary name", 1, 55, 0, 50 },
						 
						//ITEM TESTS 
						{"value-- | quality--","+5 Dexterity Vest", 10, 25, 9, 24 },
						{"value-- | quality--","+5 Dexterity Vest", 10, 51, 9, 50 },
						
						{"value-- | quality++","Aged Brie", 10, 25, 9, 26 },
						{"value-- | quality++","Aged Brie", 10, 49, 9, 50 },
						{"value-- | quality++","Aged Brie", 10, 50, 9, 50 },
						
						//must not increase greater than 50
						//##################changed characterization test on the basis of the requirements
						// characterization test was (10, 51, 9, 51 )
						{"value-- | quality++","Aged Brie", 10, 51, 9, 50 },
						
						//##################changed characterization test on the basis of the requirements
						// characterization test was (11, 51, 10, 51 )
						{"value-- | quality++","Aged Brie", 11, 51, 10, 50 },
						{"value-- | quality++","Aged Brie", 1, 43, 0, 44 },
						
						//##################changed characterization test on the basis of the requirements
						// characterization test was (0, 43, -1, 45)
						{"value-- | quality++","Aged Brie", 0, 43, -1, 44 },
						
						
						{"value -- | quality --","Elixir of the Mongoose", 10, 25, 9, 24 },
						{"value -- | quality --","Elixir of the Mongoose", 11, 25, 10, 24 },
						{"value -- | quality --","Elixir of the Mongoose", 10, 51, 9, 50 },
						{"value -- | quality --","Elixir of the Mongoose", 1, 25, 0, 24 },
						{"value -- | quality 2--","Elixir of the Mongoose", 0, 25, -1, 23 },
						
						{"value +-0 | qulaity +-0","Sulfuras, Hand of Ragnaros", 10, 25, 10, 25 },
						{"value +-0 | qulaity +-0","Sulfuras, Hand of Ragnaros", 10, 80, 10, 80 },
						{"value +-0 | qulaity +-0","Sulfuras, Hand of Ragnaros", 10, 81, 10, 81 },
						
						{"value-- | quality2++","Backstage passes to a TAFKAL80ETC concert", 10, 25, 9, 27 },
						{"value-- | quality++","Backstage passes to a TAFKAL80ETC concert", 11, 25, 10, 26 },
						{"value-- | quality+-0","Backstage passes to a TAFKAL80ETC concert", 11, 50, 10, 50 },
						
						//##################changed characterization test on the basis of the requirements
						// characterization test was (12, 51, 11, 51)
						{"value-- | quality+-0","Backstage passes to a TAFKAL80ETC concert", 12, 51, 11, 50 },
						{"value-- | quality2++","Backstage passes to a TAFKAL80ETC concert", 6, 25, 5, 27 },
						{"value-- | quality3++","Backstage passes to a TAFKAL80ETC concert", 5, 25, 4, 28 },
						{"value-- | quality3++","Backstage passes to a TAFKAL80ETC concert", 4, 25, 3, 28 },
						{"value-- | quality3++","Backstage passes to a TAFKAL80ETC concert", 1, 25, 0, 28 },
						{"value-- | quality set to 0","Backstage passes to a TAFKAL80ETC concert", 0, 25, -1, 0 },
						
						//##################changed characterization test on the basis of the requirements
						//The conjured wasnt implemented during the creation of the characterization test
						// characterization test was (10, 25, 9, 24 )
						{"value-- | quality--","Conjured Mana Cake", 10, 25, 9, 23 },
						//##################changed characterization test on the basis of the requirements
						//The conjured wasnt implemented during the creation of the characterization test
						// characterization test was ( 1, 25, 0, 24 )
						{"value-- | quality--","Conjured Mana Cake", 1, 25, 0, 23 },
						
						//Conjured usually -2 when experied -4
						//##################changed characterization test on the basis of the requirements
						//The conjured wasnt implemented during the creation of the characterization test
						// characterization test was (0, 25, -1, 23  )
						{"value-- | quality2--","Conjured Mana Cake", 0, 25, -1, 21 },
						
						{"value++ | quality++","Octocat", 10, 25, 11, 26 },
						{"value++ | quality++","Octocat", 11, 49, 12, 50 },
						{"value++ | quality++","Octocat", 11, 50, 12, 52 },
						{"value++ | quality++","Octocat", -3, -5, -2, -4 },
						
						}));
		
		return data;
	}

	String message;
	String itemName;
	int sellIn;
	int quality;
	int expectedSellIn;  
	int expectedQuality;

	Item item;

	public GildedRoseTest(String message, String itemName, int sellIn,
			int quality, int expectedSellIn, int expectedQuality) {
		this.message = message;
		this.itemName = itemName;
		this.sellIn = sellIn;
		this.quality = quality;
		this.expectedSellIn = expectedSellIn;
		this.expectedQuality = expectedQuality;
	}

	@Before
	public void setUp() {
		List<Item> items = new ArrayList<Item>();
		items.add(item = new Item(itemName, sellIn, quality));
		GildedRose.setItems(items);
	}

	@Test
	public void testQualityUpdate() {
		GildedRose.update();
		assertEquals(message + " Quality ", expectedQuality, item.getQuality());
	}

	@Test
	public void testSellInUpdate() {
		GildedRose.update();
		assertEquals(message + " SellIn", expectedSellIn, item.getSellIn());
	}
	
}
