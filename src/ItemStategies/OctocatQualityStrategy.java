package ItemStategies;

import gildedrose.Item;
import gildedrose.UpdateQualityStrategy;

public class OctocatQualityStrategy extends UpdateQualityStrategy {

	@Override
	public void computeQuality(Item item) {
		if (item.quality < 50) {
			item.quality++;
		} else {
			item.quality+=2;
		}
	}

	@Override
	public void checkBounds(Item item) {
		//do nothing
	}

}
