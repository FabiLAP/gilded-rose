package gildedrose;

import ItemStategies.*;

//import ItemStategies.AgedBrieQualityStrategy;
//import ItemStategies.BackstagePassesQualityStrategy;
//import ItemStategies.ConjuredQualityStrategy;
//import ItemStategies.OctocatQualityStrategy;
//import ItemStategies.SulfurasQualityStrategy;

public class UpdateQualityStrategy {
	
	/*
	 * factory design pattern
	 * returns the corresponding subobject or the object itself depending on the name of the item
	 */
	public static UpdateQualityStrategy factoryGetUpdateQualityStrategy(String nameOfItem){
		switch(nameOfItem){
		case "Aged Brie":
            return new AgedBrieQualityStrategy();
		case "Sulfuras, Hand of Ragnaros":
            return new SulfurasQualityStrategy();
  		case "Backstage passes to a TAFKAL80ETC concert":
  			return new BackstagePassesQualityStrategy();
  		case "Conjured Mana Cake":
  			return new ConjuredQualityStrategy();
  		case "Octocat":
  			return new OctocatQualityStrategy();
		default: 
            return new UpdateQualityStrategy();
		}
	}
	
	/*
	 * gets called by the gildedrose class
	 */
	public void updateQuality(Item item){
		this.computeQuality(item);
		this.checkBounds(item);
	}
	
	/*
	 * calculates the default quality update - gets overwritten for special items
	 */
	public void computeQuality(Item item){
		if(item.sellIn>0){
			item.quality--;			
		}
		else{
			item.quality-=2;
		}
	}
	
	/*
	 * resets the bounds to the default values (0 to 50) - gets overwritten for special items
	 */
	public void checkBounds(Item item){
		if(item.quality<0){
			item.quality = 0;
		}
		if(item.quality>50){
			item.quality = 50;
		}
	}
}