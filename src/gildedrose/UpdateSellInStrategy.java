package gildedrose;

import ItemStategies.*;

//import ItemStategies.OctocatSellInStrategy;
//import ItemStategies.SulfurasSellInStrategy;

public class UpdateSellInStrategy {

	/*
	 * factory design pattern
	 * returns the corresponding subobject or the object itself depending on the name of the item
	 */
	public static UpdateSellInStrategy factoryGetUpdateSellInStrategy(String nameOfItem){
		switch(nameOfItem){
		case "Sulfuras, Hand of Ragnaros":
            return new SulfurasSellInStrategy();
		case "Octocat":
			return new OctocatSellInStrategy();
		default:
			return new UpdateSellInStrategy();
		}
	}
	
	/*
	 * gets called by the gidedrose class
	 */
	public void updateSellIn(Item item){
		item.sellIn--;
	}
	
}
